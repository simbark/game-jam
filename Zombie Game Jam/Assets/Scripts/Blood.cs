﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blood : MonoBehaviour
{
    public bool squirt;
    public Sprite[] anim1;
    public Sprite[] anim2;
    public Sprite[] anim3;
    public Sprite[] puddles;
    SpriteRenderer sr;
    Sprite[][] anims;
    Sprite[] anim;
    int pos;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        if(squirt)
        {
            anims = new Sprite[][] {anim1, anim2, anim3};
            anim = anims[Random.Range(0,anims.Length)];
            InvokeRepeating("Loop",0f,.05f);
        }
        else sr.sprite = puddles[Random.Range(0,puddles.Length)];
    }

    void Loop()
    {
        if(pos>anim.Length-1)
        {
            CancelInvoke();
            Destroy(gameObject);
        }
        else
        {
            sr.sprite = anim[pos];
            pos++;
        }
    }
}
