﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    static Pointer cursor;
    static float speed = 2.5f;
    bool latching = false;
    float eatTime;
    float lastTurn;
    float lastMoan;
    public GameObject zombiePrefab;
    public GameObject corpsePrefab;
    public GameObject splatPrefab;
    public GameObject puddlePrefab;
    public AudioSource[] moans;
    public AudioSource[] chomps;
    public Sprite[] walk;
    int frame;
    SpriteRenderer sr;
    GameObject currentHuman;
    Vector2 lockedPos;

    void Start()
    {
        if(cursor==null) cursor = GameObject.Find("/Cursor").GetComponent<Pointer>();
        cursor.UpdateZombies(1);
        moans[Random.Range(0,moans.Length)].Play();
        sr = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();
        sr.color = Color.white;
        InvokeRepeating("Walk",0f,.1f);
    }

    /* Every frame...
       Update the radius based on mouse input from Pointer class
       Move toward cursor object and flip to face it
       Be "killing" the human during latching process until process is complete
    */
    void Update()
    {
        if(cursor!=null && transform.parent!=null)
        {
            GetComponent<CircleCollider2D>().radius = cursor.zombRad;
            if(!latching)
            {
                transform.position = Vector2.MoveTowards(transform.position,cursor.transform.position,speed*Time.deltaTime);
                transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.y);
                if(lastTurn>.1f)
                {
                    lastTurn = 0;
                    if(cursor.transform.position.x<transform.position.x) transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x)*-1,transform.localScale.y,transform.localScale.z);
                    else transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x),transform.localScale.y,transform.localScale.z);
                }
                if(lastMoan>10f)
                {
                    lastMoan = 0;
                    moans[Random.Range(0,moans.Length)].Play();
                }
            }
            else
            {
                transform.position = lockedPos;
                eatTime += Time.deltaTime;
                if(eatTime>1f)
                {
                    latching = false;
                    eatTime = 0f;
                    CancelInvoke("Squirt");
                    currentHuman.GetComponent<Blip>().DestroyBlip();
                    Destroy(currentHuman);
                    currentHuman = null;
                    Instantiate(puddlePrefab).transform.position = transform.position;
                    GameObject newZombie = Instantiate(zombiePrefab);
                    newZombie.transform.position = new Vector2(transform.position.x+.01f,transform.position.y);
                    newZombie.transform.parent = transform.parent;
                    Vector3 tempLS = newZombie.transform.localScale;
                    if(cursor.transform.position.x<transform.position.x) transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x)*-1,transform.localScale.y,transform.localScale.z);
                    if(transform.localScale.x<0) newZombie.transform.localScale = new Vector3(tempLS.x*-1,tempLS.y,tempLS.z);
                    sr.color = Color.white;
                }
            }
            lastTurn += Time.deltaTime;
            lastMoan += Time.deltaTime;
        }
    }

    /* Restrict human behavior
       Snap to human position
       Human turns opposite of zombie
       Set local variables
    */
    public void Latch(GameObject human)
    {
        sr.color = Color.clear;
        if(transform.localScale.x<0) human.transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x),transform.localScale.y,transform.localScale.z);
        else human.transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x)*-1,transform.localScale.y,transform.localScale.z);
        transform.position = human.transform.position;
        latching = true;
        InvokeRepeating("Squirt",.05f,.3f);
        currentHuman = human;
        lockedPos = transform.position;
        chomps[Random.Range(0,chomps.Length)].Play();
    }

    void Squirt() {Instantiate(splatPrefab).transform.position = transform.position;}

    public void Kill()
    {
        cursor.UpdateZombies(-1);
        transform.GetComponent<Blip>().DestroyBlip();
        CancelInvoke("Squirt");
        if(currentHuman!=null) currentHuman.GetComponent<Civilian>().Recover();
        GameObject newCorpse = Instantiate(corpsePrefab);
        newCorpse.transform.position = transform.position;
        newCorpse.transform.parent = null;
        Instantiate(splatPrefab).transform.position = transform.position;
        Instantiate(puddlePrefab).transform.position = transform.position;
        Destroy(gameObject);
    }
    
    void Walk()
    {
        if(frame>walk.Length-1) frame=0;
        else sr.sprite = walk[frame];
        frame+=1;
    }
}
