﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    Vector3 zero = new Vector3(0f,0f,0f);
    Vector3 full = new Vector3(1.5f,1.5f,1f);

    void Start()
    {
        if(SceneManager.GetActiveScene().name=="Menu")
        {
            GameObject howTo = GameObject.Find("/Canvas/HowTo");
            howTo.transform.localScale = zero;
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene("Game");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void HTP()
    {
        GameObject howTo = GameObject.Find("/Canvas/HowTo");
        if(howTo.transform.localScale==zero) howTo.transform.localScale = full;
        else howTo.transform.localScale = zero;
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
}
