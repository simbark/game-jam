﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blip : MonoBehaviour
{
    static Pointer cursor;
    static float scale = 5f;
    public GameObject blipPrefab;
    Color original;
    Transform blip;

    void Start()
    {
        if(cursor==null) cursor = GameObject.Find("/Cursor").GetComponent<Pointer>();
        blip = Instantiate(blipPrefab,GameObject.Find("/Canvas/Radar").transform).transform;
        original = blip.GetComponent<Image>().color;
        Update();
    }

    /* Every frame...
       Update blip position based on civilian position
       If blip is too far, fade out or even become invisible
    */
    void Update()
    {
        if(blip.GetComponent<RectTransform>()!=null)
        {
            Vector3 calc = (transform.position-Camera.main.transform.position)*scale;
            blip.localPosition = new Vector3(calc.x,calc.y,0);
            blip.GetComponent<Image>().color = new Color(original.r,original.g,original.b,1-blip.localPosition.magnitude/150);
            blip.GetComponent<Image>().enabled = Mathf.Abs(blip.localPosition.x)<100 && Mathf.Abs(blip.localPosition.y)<100;
        }
    }

    public void DestroyBlip()
    {
        Destroy(blip.gameObject);
    }
}
