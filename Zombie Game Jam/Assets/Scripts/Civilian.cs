﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Civilian : MonoBehaviour
{
    static Pointer cursor;
    public GameObject closestZombie;
    public GameObject puddlePrefab;
    public GameObject zombiePrefab;
    public float speed = 1f;
    public float[] range = {3.5f, 20f};
    public bool dying = false;
    public bool running = false;
    public bool faces = false;
    public AudioSource footsteps;
    public AudioSource[] femaleYell;
    public AudioSource[] maleYell;
    public Sprite idle;
    public Sprite[] runAnim;
    public Sprite[] eatAnim;
    SpriteRenderer sr;
    AudioSource[] yells;
    float lastTurn;
    float dieTime;
    int frame;

    void Start()
    {
        if(cursor==null) cursor = GameObject.Find("/Cursor").GetComponent<Pointer>();
        if(Random.Range(0,2)==1) yells = maleYell;
        else yells = femaleYell;
        sr = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();
    }

    /* Every frame...
       Civilian finds the closest zombie and their distance
       Civilian decides whether to run, stop, or be attacked
       Civilian faces toward/away from the nearest zombie
    */
    void Update()
    {
        if(cursor!=null)
        {
            if(!dying)
            {
                Vector2 tPos = new Vector2(transform.position.x,transform.position.y);
                Vector2 cPos = Vector2.zero;
                Vector2 zPos = new Vector2(17.3f,12.5f);
                foreach(GameObject z in cursor.zombies) if(z!=null)
                {   zPos = new Vector2(z.transform.position.x,z.transform.position.y);
                    if(closestZombie!=null) cPos = new Vector2(closestZombie.transform.position.x,closestZombie.transform.position.y);
                    if(closestZombie==null||((zPos-tPos).magnitude<(cPos-tPos).magnitude)) closestZombie = z;
                }
                float distToZomb = (cPos-tPos).magnitude;
                if(distToZomb<.5)
                {
                    dying = true;
                    footsteps.Stop();
                    running = false;
                    CancelInvoke();
                    frame = 0;
                    InvokeRepeating("Die",0f,.1f);
                    closestZombie.GetComponent<Zombie>().Latch(gameObject);
                    dieTime = 0f;
                }
                else if(distToZomb>range[1])
                {
                    footsteps.Stop();
                    running = false;
                    sr.sprite = idle;
                    CancelInvoke("Run");
                }
                else if(!running && distToZomb<range[0])
                {
                    footsteps.Play();
                    yells[Random.Range(0,yells.Length)].Play();
                    running = true;
                    if(runAnim.Length>0) InvokeRepeating("Run",0f,.1f);
                }
            }
            else
            {
                dieTime += Time.deltaTime;
                if(dieTime>1.5f)
                {
                    Instantiate(puddlePrefab).transform.position = transform.position;
                    GameObject newZombie = Instantiate(zombiePrefab);
                    newZombie.transform.parent = GameObject.Find("/Zombies").transform;
                    newZombie.transform.position = new Vector2(transform.position.x+.01f,transform.position.y);
                    if(cursor.transform.position.x<transform.position.x) newZombie.transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x)*-1,transform.localScale.y,transform.localScale.z);
                    transform.GetComponent<Blip>().DestroyBlip();
                    Destroy(gameObject);
                }
            }
            
            if(running)
            {
                if(dying)
                {
                    footsteps.Stop();
                    running = false;
                    CancelInvoke("Run");
                }
                transform.position = Vector2.MoveTowards(transform.position,closestZombie.transform.position,-1*speed*Time.deltaTime);
                transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.y);
                if(lastTurn>.1f)
                {
                    lastTurn = 0;
                    if(faces)
                    {
                        if(closestZombie.transform.position.x<transform.position.x) transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x)*-1,transform.localScale.y,transform.localScale.z);
                        else transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x),transform.localScale.y,transform.localScale.z);
                    }
                    else
                    {
                        if(closestZombie.transform.position.x<transform.position.x) transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x),transform.localScale.y,transform.localScale.z);
                        else transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x)*-1,transform.localScale.y,transform.localScale.z);
                    }
                }
            }
            lastTurn += Time.deltaTime;
        }
        else footsteps.Stop();
    }
    void Run()
    {
        CancelInvoke("Die");
        if(sr.sprite==runAnim[0]) sr.sprite=runAnim[1];
        else sr.sprite=runAnim[0];
    }
    void Die()
    {
        CancelInvoke("Run");
        if(frame>eatAnim.Length-1) frame=0;
        else sr.sprite = eatAnim[frame];
        frame+=1;
    }
    public void Recover()
    {
        dieTime = 0f;
        sr.sprite = idle;
        dying = false;
        CancelInvoke("Die");
    }
}
