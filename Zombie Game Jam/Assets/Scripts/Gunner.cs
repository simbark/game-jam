﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gunner : MonoBehaviour
{
    static Pointer cursor;
    public Civilian civ;
    public AudioSource shot;
    public Sprite shoot;
    public Sprite idle;
    SpriteRenderer sr;
    float lastShot;

    void Start()
    {
        transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.y);
        if(cursor==null) cursor = GameObject.Find("/Cursor").GetComponent<Pointer>();
        sr = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if(cursor!=null && !civ.dying && civ.running)
        {
            if(lastShot>1f)
            {
                lastShot = 0;
                shot.Play();
                civ.closestZombie.GetComponent<Zombie>().Kill();
                sr.sprite = shoot;
            }
            else if(lastShot>.1f) sr.sprite = idle;
            lastShot += Time.deltaTime;
        }
    }
}
