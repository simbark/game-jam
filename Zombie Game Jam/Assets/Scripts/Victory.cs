﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Victory : MonoBehaviour
{
    public Pointer cursor;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.GetComponent<Zombie>()!=null)
        {
            cursor.Victory();
            Destroy(gameObject);
        }
    }
}
