﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corpse : MonoBehaviour
{
    public Sprite[] anim;
    public SpriteRenderer sr;
    int frame;

    void Start() {InvokeRepeating("Loop",0f,.1f);}

    void Loop()
    {
        if(frame>anim.Length-1) CancelInvoke();
        else sr.sprite = anim[frame];
        frame+=1;
    }
}
