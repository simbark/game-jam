﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pointer : MonoBehaviour
{
    Text zombiesText;
    Text timer;
    Text message1;
    Text message2;
    GameObject gameOverDialogue;
    GameObject victoryDialogue;
    Transform cam;
    float panSpeed = 10f;
    float radSpeed = 1f;
    float radThresh = .025f;
    float initRad = .05f;
    float radMult = 1f;
    float runTime = 0f;
    float totalTime = 0f;
    float timeLimit = 600f;
    public int zombiesNum = 0;
    public GameObject[] zombies;
    public float zombRad;
    public AudioSource musicStart;
    public AudioSource musicLoop;
    public AudioSource win;
    public AudioSource lose;

    void Start()
    {
        Cursor.visible = false;
        cam = Camera.main.transform;
        zombiesText = GameObject.Find("/Canvas/Zombies").GetComponent<Text>();
        timer = GameObject.Find("/Canvas/Timer").GetComponent<Text>();
        gameOverDialogue = GameObject.Find("/Canvas/GameOver");
        message1 = gameOverDialogue.transform.Find("Message").gameObject.GetComponent<Text>();
        gameOverDialogue.SetActive(false);
        victoryDialogue = GameObject.Find("/Canvas/Victory");
        message2 = victoryDialogue.transform.Find("Message").gameObject.GetComponent<Text>();
        victoryDialogue.SetActive(false);
        zombRad = initRad;
    }

    /* Every frame...
       The pointer moves to the hardware cursor position
       WASD controls to pan the camera
       Left Mouse to shrink horde spread and Right Mouse to increase it
       Set/check timer and loop music after first section plays
    */
    void Update()
    {
        transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(transform.position.x,transform.position.y,0);

        if(Input.GetKey("w")&&!Input.GetKey("s")) cam.position = new Vector3(cam.position.x,cam.position.y+panSpeed*Time.deltaTime,cam.position.z);
        if(Input.GetKey("s")&&!Input.GetKey("w")) cam.position = new Vector3(cam.position.x,cam.position.y-panSpeed*Time.deltaTime,cam.position.z);
        if(Input.GetKey("a")&&!Input.GetKey("d")) cam.position = new Vector3(cam.position.x-panSpeed*Time.deltaTime,cam.position.y,cam.position.z);
        if(Input.GetKey("d")&&!Input.GetKey("a")) cam.position = new Vector3(cam.position.x+panSpeed*Time.deltaTime,cam.position.y,cam.position.z);

        if(Input.GetMouseButton(0)&&!Input.GetMouseButton(1)) radMult -= radSpeed*Time.deltaTime;
        if(Input.GetMouseButton(1)&&!Input.GetMouseButton(0)) radMult += radSpeed*Time.deltaTime;
        if(Input.GetMouseButton(1)==Input.GetMouseButton(0))
        {
            if(radMult<1) radMult += radSpeed*Time.deltaTime;
            else radMult -= radSpeed*Time.deltaTime;
            if(Mathf.Abs(1-radMult)<radThresh) radMult = 1f;
        }
        if(radMult<.5f) radMult = .5f;
        if(radMult>1.5f) radMult = 1.5f;
        if(runTime<1f) 
        {
            radMult = 1f;
            runTime += Time.deltaTime;
        }
        zombRad = initRad*radMult;

        totalTime += Time.deltaTime;
        timer.text = "Time: " + formatTime(timeLimit-totalTime);
        if(totalTime>=timeLimit)
        {
            totalTime = timeLimit;
            message1.text = "You ran out of time.";
            GameOver();
        }

        if(!musicStart.isPlaying && !musicLoop.isPlaying && totalTime>30f) musicLoop.Play();
    }

    string formatTime(float s)
    {
        int min = Mathf.FloorToInt(s/60);
        int sec = Mathf.FloorToInt(s-(min*60));
        string secS = "" + sec;
        if(sec<10) secS = "0" + sec;
        int mil = Mathf.FloorToInt((s-(min*60)-sec)*100);
        string milS = "" + mil;
        if(mil<10) milS = "0" + mil;
        return "" + min + ":" + secS + "." + milS;
    }

    /* Called to modify the zombie total variable
       Updates the HUD to reflect the variable
       Call Game Over if no zombies left
       Update the public zombies variable
    */
    public void UpdateZombies(int z)
    {
        zombiesNum += z;
        if(zombiesNum<=0) zombiesNum = 0;
        if(zombiesText==null) zombiesText = GameObject.Find("/Canvas/Zombies").GetComponent<Text>();
        zombiesText.text = "Zombies: " + zombiesNum;
        if(zombiesNum==0) GameOver();

        Transform zombieTree = GameObject.Find("/Zombies").transform;
        zombies = new GameObject[zombieTree.childCount];
        for(int i=0;i<zombies.Length;i++) zombies[i] = zombieTree.GetChild(i).gameObject;
    }

    void GameOver()
    {
        musicStart.Stop(); musicLoop.Stop();
        Cursor.visible = true;
        gameOverDialogue.SetActive(true);
        lose.Play();
        Destroy(gameObject);
    }

    public void Victory()
    {
        musicStart.Stop(); musicLoop.Stop();
        Cursor.visible = true;
        victoryDialogue.SetActive(true);
        win.Play();
        message2.text = "Final time: " + formatTime(totalTime);
        Destroy(gameObject);
    }
}
