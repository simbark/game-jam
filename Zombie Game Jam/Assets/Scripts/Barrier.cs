﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour
{
    static Pointer cursor;
    Transform txtObj;
    TextMesh numText;
    TextMesh numText2;
    GameObject closestZombie;
    public int zombiesRequired;
    public float triggerDist = 1f;
    public AudioSource crash;
    bool crashing = false;
    float degrees = 0f;
    bool left = true;

    void Start()
    {
        transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.y);
        if(cursor==null) cursor = GameObject.Find("/Cursor").GetComponent<Pointer>();
        txtObj = transform.GetChild(0);
        numText = txtObj.GetComponent<TextMesh>();
        numText2 = txtObj.GetChild(0).GetComponent<TextMesh>();
        numText.text = numText2.text = "" + zombiesRequired;
    }

    void Update()
    {
        if(cursor!=null)
        {
            if(!crashing)
            {
                foreach(GameObject z in cursor.zombies) if(z!=null)
                {
                    Vector2 zPos = new Vector2(z.transform.position.x,z.transform.position.y);
                    Vector2 tPos = new Vector2(transform.position.x,transform.position.y);
                    Vector2 cPos = Vector2.zero;
                    if(closestZombie!=null) cPos = new Vector2(closestZombie.transform.position.x,closestZombie.transform.position.y);
                    if(closestZombie==null||((zPos-tPos).magnitude<(cPos-tPos).magnitude)) closestZombie = z;
                }
                float distToZomb = (closestZombie.transform.position-transform.position).magnitude;

                if(left)
                {
                    degrees += 40*Time.deltaTime;
                    if(degrees>10) left = false;
                }
                else
                {
                    degrees -= 40*Time.deltaTime;
                    if(degrees<-10) left = true;
                }
                txtObj.eulerAngles = new Vector3(0,0,degrees);

                if(distToZomb<triggerDist && cursor.zombiesNum>=zombiesRequired)
                {
                    crashing = true;
                    crash.Play();
                    transform.GetComponent<Blip>().DestroyBlip();
                    Destroy(numText.gameObject);
                    Destroy(transform.GetComponent<Rigidbody2D>());
                    Destroy(transform.GetComponent<BoxCollider2D>());
                    Destroy(transform.GetComponent<SpriteRenderer>());
                }
            }
            else if(!crash.isPlaying) Destroy(gameObject);
        }
    }
}
